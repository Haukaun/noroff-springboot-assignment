# Noroff Assignment 3: Spring Boot API

This project is part of the Noroff accelerate program, aiming to demonstrate our understanding and implementation skills with Java, Spring Boot, Hibernate, PostgreSQL, and more. In this assignment, we've built an API to manage various entities.

## Table of Contents

- [Technologies](#technologies)
- [Setup](#setup)
- [API Endpoints](#api-endpoints)
- [Documentation](#documentation)
- [Contributions](#contributions)

## Technologies

- **Java** - Core programming language.
- **Spring Boot** - For creating the RESTful web services.
- **Spring Data JPA** - To persist data in SQL stores with Java Persistence API using Spring Data and Hibernate.
- **PostgreSQL** - Our primary relational database.
- **Lombok** - Java library tool used to reduce boilerplate code.
- **Swagger** - Tool for API design, building, documentation, and use-case execution.

## Setup

1. **Clone the repository**
```sh
git clone <repository-url>
```


2. **Setup PostgreSQL**

Ensure you have PostgreSQL installed and running. Create a database and user, and grant privileges. Update the `application.properties` in the `src/main/resources` folder with your PostgreSQL configuration.

3. **Run the application**

Navigate to the root directory of the project and run:

```sh
./mvnw spring-boot:run
```

4. **Accessing the API**

Once the server is running, the API can be accessed at `http://localhost:8080/`.

## API Endpoints

- `/movies`: Endpoint for managing movies.
- `/characters`: Endpoint for managing characters.
- `/franchises`: Endpoint for managing franchises.

## Documentation

- **Swagger UI**: We've integrated Swagger for API documentation and testing. Once the application is running, you can access the Swagger UI at `http://localhost:8080/swagger-ui.html`.

## Contributions

This project is part of Noroff's accelerate program, and contributions are not open. However, if you find any issues or have suggestions, please open an issue.