/**
 * The package contains repository interfaces for accessing movie-related data in a database.
 */
package no.noroff.accelerate.repository;

import no.noroff.accelerate.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository interface for accessing movie data in a database.
 *
 * <p>
 * The `MovieRepository` interface extends the `JpaRepository` interface provided
 * by Spring Data JPA. It provides methods for performing CRUD (Create, Read, Update, Delete)
 * operations on movie entities in the database. The entity type is `Movie`, and the
 * primary key type is `Integer`.
 * </p>
 *
 * <p>
 * The `@Repository` annotation indicates that this interface is a Spring repository component
 * and should be automatically detected and registered by Spring for bean creation and
 * database access.
 * </p>
 */
@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {
}
