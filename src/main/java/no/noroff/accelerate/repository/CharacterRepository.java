/**
 * The package contains repository interfaces for accessing character-related data in a database.
 */
package no.noroff.accelerate.repository;

import no.noroff.accelerate.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository interface for accessing character data in a database.
 *
 * <p>
 * The `CharacterRepository` interface extends the `JpaRepository` interface provided
 * by Spring Data JPA. It provides methods for performing CRUD (Create, Read, Update, Delete)
 * operations on character entities in the database. The entity type is `Character`, and the
 * primary key type is `Integer`.
 * </p>
 *
 * <p>
 * The `@Repository` annotation indicates that this interface is a Spring repository component
 * and should be automatically detected and registered by Spring for bean creation and
 * database access.
 * </p>
 */
@Repository
public interface CharacterRepository extends JpaRepository<Character, Integer> {
}

