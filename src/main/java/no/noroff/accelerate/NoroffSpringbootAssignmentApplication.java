package no.noroff.accelerate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NoroffSpringbootAssignmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(NoroffSpringbootAssignmentApplication.class, args);
    }

}
