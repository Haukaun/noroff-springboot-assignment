package no.noroff.accelerate.services.franchise;

import no.noroff.accelerate.exceptions.FranchiseNotFoundException;
import no.noroff.accelerate.exceptions.MovieNotFoundException;
import no.noroff.accelerate.models.Character;
import no.noroff.accelerate.models.Franchise;
import no.noroff.accelerate.models.Movie;
import no.noroff.accelerate.repository.FranchiseRepository;
import no.noroff.accelerate.repository.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of the FranchiseService.
 * Provides concrete implementations for CRUD operations on the Franchise entity
 * and related operations for associated movies and characters.
 */
 @Service
public class FranchiseServiceImpl implements FranchiseService{

    private final FranchiseRepository franchiseRepository;
    private final MovieRepository movieRepository;

    public FranchiseServiceImpl(FranchiseRepository franchiseRepository, MovieRepository movieRepository) {
        this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
    }

    /**
     * Retrieves a franchise by its ID.
     *
     * @param id the ID of the franchise to be fetched.
     * @return the Franchise object if found.
     * @throws FranchiseNotFoundException if the franchise is not found.
     */
    @Override
    public Franchise findById(Integer id) {
        return franchiseRepository.findById(id).orElseThrow(() -> new FranchiseNotFoundException(id));
    }

    /**
     * Retrieves all franchises present in the repository.
     *
     * @return a collection of all franchises.
     */
    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    /**
     * Adds a new franchise entity to the repository.
     *
     * @param entity the franchise to be added.
     * @return the added franchise entity.
     */
    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    /**
     * Updates an existing franchise entity in the repository.
     *
     * @param entity the franchise to be updated.
     * @return the updated franchise entity.
     */
    @Override
    public Franchise update(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    /**
     * Deletes a franchise from the repository by its ID.
     *
     * @param id the ID of the franchise to be deleted.
     */
    @Override
    public void deleteById(Integer id) {
        franchiseRepository.deleteById(id);
    }

    /**
     * Checks if a franchise exists in the repository by its ID.
     *
     * @param id the ID of the franchise.
     * @return true if the franchise exists, false otherwise.
     */
    @Override
    public boolean exists(Integer id) {
        return franchiseRepository.existsById(id);
    }


    /**
     * Retrieves all movies associated with a given franchise.
     *
     * @param id the ID of the franchise for which movies are to be fetched.
     * @return a collection of movies associated with the franchise.
     */
    @Override
    public Collection<Movie> findAllMovies(Integer id) {
        return findById(id).getMovies();
    }

    /**
     * Retrieves all characters from movies associated with a given franchise.
     *
     * @param id the ID of the franchise for which characters are to be fetched.
     * @return a collection of characters from movies of the franchise.
     */
    @Override
    public Collection<Character> findAllCharacters(Integer id) {
        Collection<Character> characters = new HashSet<>();
        for (Movie m : findAllMovies(id)) {
            characters.addAll(m.getCharacters());
        }
        return characters;
    }

    /**
     * Updates the movies associated with a given franchise.
     *
     * @param movieIds array of movie IDs to be associated with the franchise.
     * @param franchiseId the ID of the franchise for which movies are to be updated.
     * @throws MovieNotFoundException if any movie in the provided list is not found.
     */
    @Override
    public void updateMovies(int[] movieIds, int franchiseId) {
        Franchise franchise = findById(franchiseId);
        Set<Movie> movieSet = new HashSet<>();
        for (int id : movieIds) {
            movieSet.add(movieRepository.findById(id).orElseThrow(() -> new MovieNotFoundException(id)));
        }
        franchise.setMovies(movieSet);
        franchiseRepository.save(franchise);
    }
}
