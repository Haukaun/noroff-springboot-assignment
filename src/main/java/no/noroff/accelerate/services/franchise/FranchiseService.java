package no.noroff.accelerate.services.franchise;

import no.noroff.accelerate.models.Character;
import no.noroff.accelerate.models.Franchise;
import no.noroff.accelerate.models.Movie;
import no.noroff.accelerate.services.CrudService;

import java.util.Collection;

public interface FranchiseService extends CrudService<Franchise, Integer> {

    Collection<Movie> findAllMovies(Integer id);
    Collection<Character> findAllCharacters(Integer id);
    void updateMovies(int[] movieIds, int franchiseId);
}
