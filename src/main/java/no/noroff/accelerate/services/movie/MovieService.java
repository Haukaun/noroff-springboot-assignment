package no.noroff.accelerate.services.movie;

import no.noroff.accelerate.models.Character;
import no.noroff.accelerate.models.Movie;
import no.noroff.accelerate.services.CrudService;

import java.util.Collection;

public interface MovieService extends CrudService<Movie,Integer> {

    void updateCharacter(int[] character, int movieId);

    Collection<Character> findAllCharacters(Integer id);
}
