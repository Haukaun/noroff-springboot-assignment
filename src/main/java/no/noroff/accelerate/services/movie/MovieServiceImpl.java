package no.noroff.accelerate.services.movie;

import no.noroff.accelerate.exceptions.CharacterNotFoundException;
import no.noroff.accelerate.exceptions.MovieNotFoundException;
import no.noroff.accelerate.models.Character;
import no.noroff.accelerate.models.Movie;
import no.noroff.accelerate.repository.CharacterRepository;
import no.noroff.accelerate.repository.MovieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;


/**
 * Implementation of the MovieService interface.
 * Provides CRUD operations on the Movie entity and manages related operations on the associated characters.
 */
 @Service
public class MovieServiceImpl implements MovieService {
    private final Logger logger = LoggerFactory.getLogger(MovieServiceImpl.class);
    private final MovieRepository movieRepository;

    private final CharacterRepository characterRepository;

    public MovieServiceImpl(MovieRepository movieRepository, CharacterRepository characterRepository) {
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
    }

    /**
     * Retrieves a movie by its ID.
     *
     * @param integer the ID of the movie to be fetched.
     * @return the Movie object if found.
     * @throws MovieNotFoundException if the movie is not found.
     */
    @Override
    public Movie findById(Integer integer) {
        return movieRepository.findById(integer).orElseThrow(() -> new MovieNotFoundException(integer));
    }


    /**
     * Retrieves all movies present in the repository.
     *
     * @return a collection of all movies.
     */
    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }


    /**
     * Adds a new movie entity to the repository.
     *
     * @param entity the movie to be added.
     * @return the added movie entity.
     */
    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }

    /**
     * Updates an existing movie entity in the repository.
     *
     * @param entity the movie to be updated.
     * @return the updated movie entity.
     */
    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
    }


    /**
     * Deletes a movie from the repository by its ID.
     * Ensures that associated entities like franchise and characters are properly unlinked.
     *
     * @param integer the ID of the movie to be deleted.
     */
    @Override
    public void deleteById(Integer integer) {
        Movie movie = findById(integer);

        movie.setFranchise(null);
        movie.getCharacters().forEach(s -> s.setMovies(null));

        movieRepository.delete(movie);
    }

    /**
     * Checks if a movie exists in the repository by its ID.
     *
     * @param integer the ID of the movie.
     * @return true if the movie exists, false otherwise.
     */
    @Override
    public boolean exists(Integer integer) {
        return movieRepository.existsById(integer);
    }


    /**
     * Retrieves all characters associated with a given movie.
     *
     * @param id the ID of the movie for which characters are to be fetched.
     * @return a collection of characters associated with the movie.
     */
    @Override
    public Collection<Character> findAllCharacters(Integer id) {
        return findById(id).getCharacters();
    }

    /**
     * Updates the characters associated with a given movie.
     *
     * @param character array of character IDs to be associated with the movie.
     * @param movieId the ID of the movie for which characters are to be updated.
     * @throws CharacterNotFoundException if any character in the provided list is not found.
     */
    @Override
    public void updateCharacter(int[] character, int movieId){

        Movie movie = findById(movieId);

        Set<Character> characterList = new HashSet<>();

        for(int id: character){
            characterList.add(characterRepository.findById(id).orElseThrow(()-> new CharacterNotFoundException(id)));
        }

        movie.setCharacters(characterList);
        movieRepository.save(movie);
    }
}
