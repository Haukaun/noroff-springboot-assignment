package no.noroff.accelerate.services;

import no.noroff.accelerate.models.Character;

import java.util.Collection;

/**
 * An interface defining standard CRUD operations for services that interact with various entities.
 *
 * @param <T>  The type of entity the service will manage.
 * @param <ID> The type of ID the entity has.
 */
public interface CrudService<T, ID> {
    T findById(ID id);

    Collection<T> findAll();

    T add(T entity);

    T update(T entity);

    void deleteById(ID id);

    boolean exists(ID id);
}
