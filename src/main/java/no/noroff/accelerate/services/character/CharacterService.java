package no.noroff.accelerate.services.character;

import no.noroff.accelerate.models.Character;
import no.noroff.accelerate.services.CrudService;

public interface CharacterService extends CrudService<Character, Integer> {

}
