package no.noroff.accelerate.services.character;


import no.noroff.accelerate.exceptions.CharacterNotFoundException;
import no.noroff.accelerate.repository.CharacterRepository;
import no.noroff.accelerate.models.Character;
import org.springframework.stereotype.Service;

import java.util.Collection;


/**
 * Implementation of the CharacterService.
 * Provides concrete implementations for CRUD operations on the Character entity.
 */
@Service
public class CharacterServiceImpl implements CharacterService{

    private final CharacterRepository characterRepository;


    public CharacterServiceImpl(CharacterRepository characterRepository){
        this.characterRepository = characterRepository;
    }


    /**
     * Retrieves a character by its ID.
     *
     * @param id the ID of the character to be fetched.
     * @return the Character object if found.
     * @throws CharacterNotFoundException if the character is not found.
     */
    @Override
    public Character findById(Integer id) {
        return characterRepository.findById(id).orElseThrow(() -> new CharacterNotFoundException(id));
    }

    /**
     * Retrieves all characters present in the repository.
     *
     * @return a collection of all characters.
     */
    @Override
    public Collection<Character> findAll() {
        return characterRepository.findAll();
    }

    /**
     * Adds a new character entity to the repository.
     *
     * @param entity the character to be added.
     * @return the added character entity.
     */
    @Override
    public Character add(Character entity) {
        return characterRepository.save(entity);
    }

    /**
     * Updates an existing character entity in the repository.
     *
     * @param entity the character to be updated.
     * @return the updated character entity.
     */
    @Override
    public Character update(Character entity) {
        return characterRepository.save(entity);
    }

    /**
     * Deletes a character from the repository by its ID.
     *
     * @param id the ID of the character to be deleted.
     */
    @Override
    public void deleteById(Integer id) {
        characterRepository.deleteById(id);
    }

    /**
     * Checks if a character exists in the repository by its ID.
     *
     * @param id the ID of the character.
     * @return true if the character exists, false otherwise.
     */
    @Override
    public boolean exists(Integer id) {
        return characterRepository.existsById(id);
    }


}
