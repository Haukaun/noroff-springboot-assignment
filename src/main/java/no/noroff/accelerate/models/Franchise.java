package no.noroff.accelerate.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import java.util.Set;

/**
 * Represents a franchise in the movie database.
 *
 * <p>
 * The `Franchise` class is annotated as an entity, indicating that it can be persisted
 * to a database using Jakarta Persistence. It includes fields for storing information
 * about a franchise, such as its name, description, and the set of movies associated with it.
 * </p>
 *
 * <p>
 * This class uses the Lombok library to automatically generate getter and setter methods
 * for its fields, reducing boilerplate code.
 * </p>
 *
 * @Entity - Indicates that instances of this class are entities and can be managed
 * by the Jakarta Persistence provider.
 * @Getter - Automatically generates getter methods for all fields.
 * @Setter - Automatically generates setter methods for all fields.
 */
@Entity
@Getter
@Setter
public class Franchise {

    /**
     * The unique identifier for the franchise.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * The name of the franchise.
     *
     * @Column - Specifies the mapping of this field to a database column.
     * @length - Sets the maximum length of the database column to 50 characters.
     * @nullable - Indicates that this column cannot have a null value.
     */
    @Column(length = 50, nullable = false)
    private String name;

    /**
     * A description of the franchise.
     */
    private String description;

    /**
     * The set of movies associated with this franchise.
     *
     * @OneToMany - Indicates a one-to-many relationship between franchises and movies,
     * with "franchise" as the mappedBy attribute specifying the owning side of the relationship.
     */
    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movies;
}
