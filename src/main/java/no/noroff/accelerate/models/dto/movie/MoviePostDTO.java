package no.noroff.accelerate.models.dto.movie;

import lombok.Getter;
import lombok.Setter;

/**
 * Represents a Data Transfer Object (DTO) for creating new movie entries.
 *
 * <p>
 * The `MoviePostDTO` class is used for transferring movie-related data when creating
 * new movie entries in an application. It includes fields for the essential movie
 * information, such as title, genre, release year, director, picture URL, and trailer URL,
 * which can be used to create a new movie in the system.
 * </p>
 *
 * <p>
 * This class uses the Lombok library to automatically generate getter and setter methods
 * for its fields, reducing boilerplate code.
 * </p>
 *
 * @Getter - Automatically generates getter methods for all fields.
 * @Setter - Automatically generates setter methods for all fields.
 */
@Getter
@Setter
public class MoviePostDTO {

    /**
     * The title of the movie.
     */
    private String title;

    /**
     * The genre of the movie.
     */
    private String genre;

    /**
     * The release year of the movie.
     */
    private int releaseYear;

    /**
     * The director of the movie.
     */
    private String director;

    /**
     * The URL of the movie's picture.
     */
    private String pictureUrl;

    /**
     * The URL of the movie's trailer.
     */
    private String trailerUrl;
}
