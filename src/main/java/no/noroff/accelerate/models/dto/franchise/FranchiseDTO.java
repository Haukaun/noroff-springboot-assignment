package no.noroff.accelerate.models.dto.franchise;

import lombok.Getter;
import lombok.Setter;
import java.util.Set;

/**
 * Represents a Data Transfer Object (DTO) for franchise information.
 *
 * <p>
 * The `FranchiseDTO` class is used for transferring franchise-related data between different
 * layers of an application, such as from a service to a controller. It includes fields for
 * essential franchise information, including the franchise's ID, name, description, and a set
 * of movie IDs indicating the movies associated with the franchise.
 * </p>
 *
 * <p>
 * This class uses the Lombok library to automatically generate getter and setter methods
 * for its fields, reducing boilerplate code.
 * </p>
 *
 * @Getter - Automatically generates getter methods for all fields.
 * @Setter - Automatically generates setter methods for all fields.
 */
@Getter
@Setter
public class FranchiseDTO {

    /**
     * The unique identifier for the franchise.
     */
    private int id;

    /**
     * The name of the franchise.
     */
    private String name;

    /**
     * A description of the franchise.
     */
    private String description;

    /**
     * The set of movie IDs indicating the movies associated with the franchise.
     */
    private Set<Integer> movies;
}
