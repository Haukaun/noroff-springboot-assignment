package no.noroff.accelerate.models.dto.movie;

import lombok.Getter;
import lombok.Setter;
import java.util.Set;

/**
 * Represents a Data Transfer Object (DTO) for movie information.
 *
 * <p>
 * The `MovieDTO` class is used for transferring movie-related data between different
 * layers of an application, such as from a service to a controller. It includes fields for
 * essential movie information, including the movie's ID, title, genre, release year, director,
 * picture URL, trailer URL, associated franchise ID, and a set of character IDs indicating
 * the characters appearing in the movie.
 * </p>
 *
 * <p>
 * This class uses the Lombok library to automatically generate getter and setter methods
 * for its fields, reducing boilerplate code.
 * </p>
 *
 * @Getter - Automatically generates getter methods for all fields.
 * @Setter - Automatically generates setter methods for all fields.
 */
@Getter
@Setter
public class MovieDTO {

    /**
     * The unique identifier for the movie.
     */
    private int id;

    /**
     * The title of the movie.
     */
    private String title;

    /**
     * The genre of the movie.
     */
    private String genre;

    /**
     * The release year of the movie.
     */
    private int releaseYear;

    /**
     * The director of the movie.
     */
    private String director;

    /**
     * The URL of the movie's picture.
     */
    private String pictureUrl;

    /**
     * The URL of the movie's trailer.
     */
    private String trailerUrl;

    /**
     * The ID of the franchise to which the movie belongs.
     */
    private Integer franchise;

    /**
     * The set of character IDs indicating the characters appearing in the movie.
     */
    private Set<Integer> characters;
}
