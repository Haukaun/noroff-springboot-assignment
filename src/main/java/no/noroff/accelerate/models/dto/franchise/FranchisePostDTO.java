package no.noroff.accelerate.models.dto.franchise;

import lombok.Getter;
import lombok.Setter;

/**
 * Represents a Data Transfer Object (DTO) for creating new franchise entries.
 *
 * <p>
 * The `FranchisePostDTO` class is used for transferring franchise-related data when creating
 * new franchise entries in an application. It includes fields for the essential franchise
 * information, such as name and description, which can be used to create a new franchise in the system.
 * </p>
 *
 * <p>
 * This class uses the Lombok library to automatically generate getter and setter methods
 * for its fields, reducing boilerplate code.
 * </p>
 *
 * @Getter - Automatically generates getter methods for all fields.
 * @Setter - Automatically generates setter methods for all fields.
 */
@Getter
@Setter
public class FranchisePostDTO {

    /**
     * The name of the franchise.
     */
    private String name;

    /**
     * A description of the franchise.
     */
    private String description;
}
