package no.noroff.accelerate.models.dto.character;

import lombok.Getter;
import lombok.Setter;
import java.util.Set;

/**
 * Represents a Data Transfer Object (DTO) for character information.
 *
 * <p>
 * The `CharacterDTO` class is used for transferring character-related data between different
 * layers of an application, such as from a service to a controller. It includes fields for
 * essential character information, including the character's ID, full name, alias, gender,
 * picture URL, and a set of movie IDs indicating the movies in which the character appears.
 * </p>
 *
 * <p>
 * This class uses the Lombok library to automatically generate getter and setter methods
 * for its fields, reducing boilerplate code.
 * </p>
 *
 * @Getter - Automatically generates getter methods for all fields.
 * @Setter - Automatically generates setter methods for all fields.
 */
@Getter
@Setter
public class CharacterDTO {

    /**
     * The unique identifier for the character.
     */
    private int id;

    /**
     * The full name of the character.
     */
    private String fullName;

    /**
     * The alias or nickname of the character.
     */
    private String alias;

    /**
     * The gender of the character.
     */
    private String gender;

    /**
     * The URL of the character's picture.
     */
    private String pictureUrl;

    /**
     * The set of movie IDs indicating the movies in which the character appears.
     */
    private Set<Integer> movies;
}
