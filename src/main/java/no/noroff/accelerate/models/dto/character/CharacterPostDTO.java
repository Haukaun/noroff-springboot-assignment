package no.noroff.accelerate.models.dto.character;

import lombok.Getter;
import lombok.Setter;

/**
 * Represents a Data Transfer Object (DTO) for creating new character entries.
 *
 * <p>
 * The `CharacterPostDTO` class is used for transferring character-related data when creating
 * new character entries in an application. It includes fields for the essential character
 * information, such as full name, alias, gender, and picture URL, which can be used to create
 * a new character in the system.
 * </p>
 *
 * <p>
 * This class uses the Lombok library to automatically generate getter and setter methods
 * for its fields, reducing boilerplate code.
 * </p>
 *
 * @Getter - Automatically generates getter methods for all fields.
 * @Setter - Automatically generates setter methods for all fields.
 */
@Getter
@Setter
public class CharacterPostDTO {

    /**
     * The full name of the character.
     */
    private String fullName;

    /**
     * The alias or nickname of the character.
     */
    private String alias;

    /**
     * The gender of the character.
     */
    private String gender;

    /**
     * The URL of the character's picture.
     */
    private String pictureUrl;
}
