/**
 * The package contains entity classes for representing movies in a movie database.
 * These classes are used to interact with a database using the Jakarta Persistence API.
 */
package no.noroff.accelerate.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import java.util.Set;

/**
 * Represents a movie in the movie database.
 *
 * <p>
 * The `Movie` class is annotated as an entity, indicating that it can be persisted
 * to a database using Jakarta Persistence. It includes fields for storing information
 * about a movie, such as its title, genre, release year, director, picture URL, trailer URL,
 * associated franchise, and the set of characters in the movie.
 * </p>
 *
 * <p>
 * This class uses the Lombok library to automatically generate getter and setter methods
 * for its fields, reducing boilerplate code.
 * </p>
 *
 * @Entity - Indicates that instances of this class are entities and can be managed
 * by the Jakarta Persistence provider.
 * @Getter - Automatically generates getter methods for all fields.
 * @Setter - Automatically generates setter methods for all fields.
 */
@Entity
@Getter
@Setter
public class Movie {

    /**
     * The unique identifier for the movie.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * The title of the movie.
     *
     * @Column - Specifies the mapping of this field to a database column.
     * @length - Sets the maximum length of the database column to 50 characters.
     * @unique - Ensures that the values in this column are unique across all records.
     * @nullable - Indicates that this column cannot have a null value.
     */
    @Column(length = 50, unique = true, nullable = false)
    private String title;

    /**
     * The genre of the movie.
     *
     * @Column - Specifies the mapping of this field to a database column.
     * @length - Sets the maximum length of the database column to 50 characters.
     */
    @Column(length = 50)
    private String genre;

    /**
     * The release year of the movie.
     */
    private int releaseYear;

    /**
     * The director of the movie.
     *
     * @Column - Specifies the mapping of this field to a database column.
     * @length - Sets the maximum length of the database column to 50 characters.
     */
    @Column(length = 50)
    private String director;

    /**
     * The URL of the movie's picture.
     */
    private String pictureUrl;

    /**
     * The URL of the movie's trailer.
     */
    private String trailerUrl;

    /**
     * The franchise to which the movie belongs.
     *
     * @ManyToOne - Indicates a many-to-one relationship between movies and franchises.
     * @JoinColumn - Specifies the name of the foreign key column in the database table.
     */
    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;

    /**
     * The set of characters appearing in the movie.
     *
     * @ManyToMany - Indicates a many-to-many relationship between movies and characters.
     */
    @ManyToMany
    private Set<Character> characters;
}
