package no.noroff.accelerate.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import java.util.Set;

/**
 * Represents a character in the movie database.
 *
 * <p>
 * The `Character` class is annotated as an entity, indicating that it can be persisted
 * to a database using Jakarta Persistence. It includes fields for storing information
 * about a character, such as their full name, alias, gender, picture URL, and the set
 * of movies in which they appear.
 * </p>
 *
 * <p>
 * This class uses the Lombok library to automatically generate getter and setter methods
 * for its fields, reducing boilerplate code.
 * </p>
 *
 * @Entity - Indicates that instances of this class are entities and can be managed
 * by the Jakarta Persistence provider.
 * @Getter - Automatically generates getter methods for all fields.
 * @Setter - Automatically generates setter methods for all fields.
 */
@Entity
@Getter
@Setter
public class Character {

    /**
     * The unique identifier for the character.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * The full name of the character.
     *
     * @Column - Specifies the mapping of this field to a database column.
     * @length - Sets the maximum length of the database column to 50 characters.
     * @nullable - Indicates that this column cannot have a null value.
     * @unique - Ensures that the values in this column are unique across all records.
     */
    @Column(length = 50, nullable = false, unique = true)
    private String fullName;

    /**
     * The alias or nickname of the character.
     *
     * @Column - Specifies the mapping of this field to a database column.
     * @length - Sets the maximum length of the database column to 50 characters.
     */
    @Column(length = 50)
    private String alias;

    /**
     * The gender of the character.
     *
     * @Column - Specifies the mapping of this field to a database column.
     * @length - Sets the maximum length of the database column to 10 characters.
     */
    @Column(length = 10)
    private String gender;

    /**
     * The URL of the character's picture.
     */
    private String pictureUrl;

    /**
     * The set of movies in which the character appears.
     *
     * @ManyToMany - Indicates a many-to-many relationship between characters and movies.
     */
    @ManyToMany
    private Set<Movie> movies;
}
