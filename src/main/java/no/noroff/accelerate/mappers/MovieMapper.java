package no.noroff.accelerate.mappers;


import no.noroff.accelerate.models.Character;
import no.noroff.accelerate.models.Movie;
import no.noroff.accelerate.models.dto.movie.MovieDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import no.noroff.accelerate.models.Franchise;
import no.noroff.accelerate.models.dto.movie.MoviePostDTO;
import no.noroff.accelerate.services.character.CharacterService;
import no.noroff.accelerate.services.franchise.FranchiseService;

import org.springframework.beans.factory.annotation.Autowired;


import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Mapper class responsible for converting between Movie, MovieDTO, and MoviePostDTO entities.
 * Uses MapStruct for object mapping and Spring's component model for bean injection.
 */
 @Mapper(componentModel = "spring")
public abstract class MovieMapper {

    @Autowired
    private CharacterService characterService;

    @Autowired
    private FranchiseService franchiseService;


    /**
     * Maps a Movie entity to MovieDTO.
     *
     * @param movie The Movie entity.
     * @return The mapped MovieDTO.
     */
    @Mapping(target = "franchise", source = "franchise.id")
    @Mapping(target = "characters", qualifiedByName = "characterToCharacterId")
    public abstract MovieDTO movieToMovieDTO(Movie movie);


    /**
     * Maps a collection of Movie entities to a collection of MovieDTOs.
     *
     * @param movie The collection of Movie entities.
     * @return The mapped collection of MovieDTOs.
     */
    public abstract Collection<MovieDTO> movieToMovieDTO(Collection<Movie> movie);


    /**
     * Maps a MoviePostDTO to a Movie entity.
     *
     * @param moviePostDto The MoviePostDTO object.
     * @return The mapped Movie entity.
     */
    public abstract Movie moviePostDTOToMovie(MoviePostDTO moviePostDto);



    /**
     * Maps a MovieDTO to a Movie entity.
     *
     * @param movie The MovieDTO object.
     * @return The mapped Movie entity.
     */
    @Mapping(target = "franchise" , qualifiedByName = "franchiseIdToFranchise")
    @Mapping(target = "characters", qualifiedByName = "characterIdToCharacter")
    public abstract Movie movieDTOToMovie(MovieDTO movie);


    /**
     * Maps a set of Character entities to a set of their IDs.
     *
     * @param value The set of Character entities.
     * @return The set of mapped character IDs.
     */
    @Named(value = "characterToCharacterId")
    public Set<Integer> mapCharacterToId(Set<Character> value) {
        if(value == null)
            return null;
        return value.stream().map(Character::getId).collect(Collectors.toSet());
    }

    /**
     * Maps a set of character IDs to a set of Character entities.
     *
     * @param value The set of character IDs.
     * @return The set of mapped Character entities.
     */
    @Named(value = "characterIdToCharacter")
    public Set<Character> mapIdToCharacter(Set<Integer> value){
        if(value == null) {
            return null;
        }
        return value.stream().map(c -> characterService.findById(c)).collect(Collectors.toSet());
    }

    /**
     * Maps a franchise ID to a Franchise entity.
     *
     * @param id The franchise ID.
     * @return The mapped Franchise entity.
     */
    @Named(value = "franchiseIdToFranchise")
    public Franchise mapIdToFranchise(Integer id){
        if(id==null){
            return null;
        }
        return franchiseService.findById(id);
    }
}

