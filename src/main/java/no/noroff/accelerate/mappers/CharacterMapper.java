package no.noroff.accelerate.mappers;


import no.noroff.accelerate.models.Character;
import no.noroff.accelerate.models.Movie;
import no.noroff.accelerate.models.dto.character.CharacterDTO;
import no.noroff.accelerate.models.dto.character.CharacterPostDTO;
import no.noroff.accelerate.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Mapper component for transforming between Character, CharacterDTO, and CharacterPostDTO objects.
 * This mapper utilizes the MapStruct framework to automate the conversion process and ensure type safety.
 */
@Mapper(componentModel = "spring")
public abstract class CharacterMapper {
    @Autowired
    protected MovieService movieService;



    /**
     * Converts a Character entity to a CharacterDTO.
     *
     * @param character The Character entity to be transformed.
     * @return The transformed CharacterDTO.
     */
    @Mapping(target = "movies", qualifiedByName = "mapMovieId")
    public abstract CharacterDTO characterToCharacterDTO(Character character);

    /**
     * Converts a CharacterDTO to a Character entity.
     *
     * @param characterDTO The CharacterDTO to be transformed.
     * @return The transformed Character entity.
     */
    @Mapping(target = "movies", qualifiedByName = "unMapMovieId")
    public abstract Character characterDTOToCharacter(CharacterDTO characterDTO);

    /**
     * Converts a CharacterPostDTO to a Character entity.
     *
     * @param characterPostDTO The CharacterPostDTO to be transformed.
     * @return The transformed Character entity.
     */
    public abstract Character characterPostDTOToCharacter(CharacterPostDTO characterPostDTO);

    /**
     * Converts a collection of Character entities to a collection of CharacterDTOs.
     *
     * @param characterCollection The collection of Character entities to be transformed.
     * @return The collection of transformed CharacterDTOs.
     */
    public abstract Collection<CharacterDTO> characterToCharacterDTO(Collection<Character> characterCollection);

    /**
     * Maps a set of Movie entities to a set of their corresponding IDs.
     *
     * @param id The set of Movie entities.
     * @return The set of corresponding movie IDs.
     */
    @Named("mapMovieId")
    Set<Integer> mapMovieId(Set<Movie> id) {
        return id.stream().map(Movie::getId).collect(Collectors.toSet());
    }

    /**
     * Maps a set of movie IDs to their corresponding Movie entities.
     *
     * @param id The set of movie IDs.
     * @return The set of corresponding Movie entities.
     */
    @Named("unMapMovieId")
    Set<Movie> unMapMovieId(Set<Integer> id) {
        return id.stream().map(movieService::findById).collect(Collectors.toSet());
    }
}
