package no.noroff.accelerate.mappers;

import no.noroff.accelerate.models.Franchise;
import no.noroff.accelerate.models.Movie;
import no.noroff.accelerate.models.dto.franchise.FranchiseDTO;
import no.noroff.accelerate.models.dto.franchise.FranchisePostDTO;
import no.noroff.accelerate.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Mapper class responsible for converting between Franchise, FranchiseDTO, and FranchisePostDTO entities.
 * Uses MapStruct for object mapping and Spring's component model for bean injection.
 */
 @Mapper(componentModel = "spring")
public abstract class FranchiseMapper {
    @Autowired
    protected MovieService movieService;

    /**
     * Maps a Franchise entity to FranchiseDTO.
     *
     * @param franchise The Franchise entity.
     * @return The mapped FranchiseDTO.
     */
    @Mapping(target = "movies", qualifiedByName = "mapMovieID")
    public abstract FranchiseDTO franchiseToFranchiseDTO(Franchise franchise);


    /**
     * Maps a FranchiseDTO to a Franchise entity.
     *
     * @param franchiseDTO The FranchiseDTO object.
     * @return The mapped Franchise entity.
     */
    @Mapping(target = "movies",qualifiedByName = "unmapMovieID")
    public abstract Franchise franchiseDTOToFranchise(FranchiseDTO franchiseDTO);

    /**
     * Maps a FranchisePostDTO to a Franchise entity.
     *
     * @param franchisePostDTO The FranchisePostDTO object.
     * @return The mapped Franchise entity.
     */
    public abstract Franchise franchisePostDTOToFranchise(FranchisePostDTO franchisePostDTO);

    /**
     * Maps a collection of Franchise entities to a collection of FranchiseDTOs.
     *
     * @param franchiseCollection The collection of Franchise entities.
     * @return The mapped collection of FranchiseDTOs.
     */
    public abstract Collection<FranchiseDTO> franchiseToFranchiseDTO(Collection<Franchise> franchiseCollection);


    /**
     * Maps a set of Movie entities to a set of their IDs.
     *
     * @param movie The set of Movie entities.
     * @return The set of mapped movie IDs.
     */
    @Named("mapMovieID")
    Set<Integer> mapMovieID(Set<Movie> movie) {
        if (movie == null)
            return null;
        return movie.stream()
                .map(Movie::getId)
                .collect(Collectors.toSet());
    }

    /**
     * Maps a set of movie IDs to a set of Movie entities.
     *
     * @param movie The set of movie IDs.
     * @return The set of mapped Movie entities.
     */
    @Named("unmapMovieID")
    Set<Movie> unmapMovieID(Set<Integer> movie) {
        if (movie == null)
            return null;
        return movie.stream()
                .map(movieService::findById)
                .collect(Collectors.toSet());

    }
}