package no.noroff.accelerate.exceptions;

import jakarta.persistence.EntityNotFoundException;

/**
 * Custom exception thrown when a Character entity is not found based on its ID.
 */
public class CharacterNotFoundException extends EntityNotFoundException {

    public CharacterNotFoundException(int id) {
        super("Character does not exist with ID: " + id);
    }
}
