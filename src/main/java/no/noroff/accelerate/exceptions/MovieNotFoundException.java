package no.noroff.accelerate.exceptions;

import jakarta.persistence.EntityNotFoundException;

/**
 * Custom exception thrown when a Movie entity is not found based on its ID.
 */
public class MovieNotFoundException extends EntityNotFoundException {

    public MovieNotFoundException(int id) {
        super("Movie does not exist with ID: " + id);
    }

}
