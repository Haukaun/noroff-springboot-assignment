package no.noroff.accelerate.exceptions;

import jakarta.persistence.EntityNotFoundException;

/**
 * Custom exception thrown when a Franchise entity is not found based on its ID.
 */
public class FranchiseNotFoundException  extends EntityNotFoundException {

    public FranchiseNotFoundException(int id) {
        super("Franchise does not exist with ID: " + id);
    }
}
