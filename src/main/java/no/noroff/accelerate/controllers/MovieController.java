package no.noroff.accelerate.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate.exceptions.CharacterNotFoundException;
import no.noroff.accelerate.mappers.MovieMapper;
import no.noroff.accelerate.exceptions.MovieNotFoundException;
import no.noroff.accelerate.models.Movie;
import no.noroff.accelerate.models.dto.movie.MovieDTO;
import no.noroff.accelerate.models.dto.movie.MoviePostDTO;
import no.noroff.accelerate.services.movie.MovieService;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping(path = "api/v1/movies")
public class MovieController {

    private final MovieService movieService;

    private final MovieMapper movieMapper;

    public MovieController(MovieService movieService, MovieMapper movieMapper) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
    }

    @GetMapping
    @Operation(summary = "Gets all movies")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                array = @ArraySchema(schema = @Schema(implementation = MovieDTO.class)))
                    }
            )
    })
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(
                movieMapper.movieToMovieDTO(
                        movieService.findAll()
                )
        );
    }

    @GetMapping("{id}")
    @Operation(summary = "Gets a movie by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = MovieDTO.class))
                    }
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content(mediaType = "application/json",  schema = @Schema(implementation = ProblemDetail.class))
            )
    })
    public ResponseEntity<?> findById(@PathVariable int id) {
        try {
            return ResponseEntity.ok().body(movieMapper.movieToMovieDTO(movieService.findById(id)));
        } catch (MovieNotFoundException e){
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    @Operation(summary = "Adds a new movie")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Created",
                    content = @Content
            )
    })
    public ResponseEntity<?> add(@RequestBody MoviePostDTO movie) throws URISyntaxException {

        Movie mov = movieService.add(movieMapper.moviePostDTOToMovie(movie));
        URI uri =  new URI("api/v1/movies/" + mov.getId());
        return ResponseEntity.created(uri).build();
    }


    @PutMapping("{id}")
    @Operation(summary = "Updates a movie")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Success",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = MovieDTO.class))
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    public ResponseEntity<?> update(@RequestBody MovieDTO entity, @PathVariable int id) {
       if(id != entity.getId()){
           return ResponseEntity.badRequest().build();
       }
        movieService.update(movieMapper.movieDTOToMovie(entity));
        return ResponseEntity.noContent().build();
    }


    @DeleteMapping("{id}")
    @Operation(summary = "Delete by id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Success",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = MovieDTO.class))
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    public ResponseEntity<?> deleteById(@PathVariable int id){
        try {
            movieService.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (MovieNotFoundException e){
            return ResponseEntity.notFound().build();
        }
    }


    @Operation(summary = "Update all character in a movie")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Success",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = MovieDTO.class))
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    @PutMapping("{id}/characters")
    public ResponseEntity<?> updateCharactersInMovie(@RequestBody int[] character, @PathVariable int id){
        try{
            movieService.updateCharacter(character, id);
            return ResponseEntity.ok().build();
        } catch (MovieNotFoundException | CharacterNotFoundException e){
            return ResponseEntity.notFound().build();
        } catch (Exception e){
            return ResponseEntity.badRequest().build();
        }
    }

    @Operation(summary = "Find all character in movie")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = MovieDTO.class))
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    @GetMapping("{id}/characters")
    public ResponseEntity<?> findAllCharters(@PathVariable int id){
        try {
            movieService.findAllCharacters(id);
            return ResponseEntity.ok().build();
        } catch (MovieNotFoundException e){
            return ResponseEntity.notFound().build();
        }
    }
}
