package no.noroff.accelerate.controllers;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate.exceptions.CharacterNotFoundException;
import no.noroff.accelerate.exceptions.MovieNotFoundException;
import no.noroff.accelerate.mappers.FranchiseMapper;
import no.noroff.accelerate.models.Character;
import no.noroff.accelerate.models.Franchise;
import no.noroff.accelerate.models.dto.franchise.FranchiseDTO;
import no.noroff.accelerate.models.dto.franchise.FranchisePostDTO;
import no.noroff.accelerate.models.dto.movie.MovieDTO;
import no.noroff.accelerate.services.franchise.FranchiseService;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping(path = "api/v1/franchise")
public class FranchiseController {
    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;

    public FranchiseController(FranchiseService franchiseService, FranchiseMapper franchiseMapper) {
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
    }

    @GetMapping
    @Operation(summary = "Gets all franchises")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = FranchiseDTO.class)))
                    }
            )
    })
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(
                franchiseMapper.franchiseToFranchiseDTO(
                        franchiseService.findAll()
                )
        );
    }

    @GetMapping("{id}")
    @Operation(summary = "Gets a franchise by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = FranchiseDTO.class))
                    }
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))
            )
    })
    public ResponseEntity<?> findById(@PathVariable int id) {
        return ResponseEntity.ok(
                franchiseMapper.franchiseToFranchiseDTO(
                        franchiseService.findById(id)
                )
        );
    }

    @PostMapping
    @Operation(summary = "Adds a new franchise")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Created",
                    content = @Content
            )
    })
    public ResponseEntity<?> add(@RequestBody FranchisePostDTO franchisePostDTO) throws URISyntaxException {
        franchiseService.add(franchiseMapper.franchisePostDTOToFranchise(franchisePostDTO));
        URI uri = new URI("api/v1/franchise/" + 1);
        return ResponseEntity.created(uri).build();
    }

    @PutMapping("{id}")
    @Operation(summary = "Updates a franchise")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    public ResponseEntity<?> update(@RequestBody FranchiseDTO entity, @PathVariable int id) {
        if (id != entity.getId())
            return ResponseEntity.badRequest().build();
        franchiseService.update(franchiseMapper.franchiseDTOToFranchise(entity));
        return ResponseEntity.noContent().build();
    }
    @DeleteMapping("{id}")
    @Operation(summary = "Deletes a character")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    public ResponseEntity<?> delete(@PathVariable int id ) {
        if(franchiseService.exists(id)){
            Franchise f = franchiseService.findById(id);
            f.getMovies().removeIf(movie ->{
              if(movie.getFranchise().getId() == id){
                    movie.setFranchise(null);
                }
                return false;
            });
            franchiseService.deleteById(id);
            return ResponseEntity.noContent().build();
        }else {
            return ResponseEntity.notFound().build();
        }
    }

    @Operation(summary = "Update all movies in a franchise")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Success",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = FranchiseDTO.class))
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    @PutMapping("{id}/movies")
    public ResponseEntity<?> updateMoviesInFranchise(@RequestBody int[] movies, @PathVariable int id){
        try{
            franchiseService.updateMovies(movies, id);
            return ResponseEntity.ok().build();
        } catch (MovieNotFoundException | CharacterNotFoundException e){
            return ResponseEntity.notFound().build();
        } catch (Exception e){
            return ResponseEntity.badRequest().build();
        }
    }

    @Operation(summary = "Find all movies in franchise")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = FranchiseDTO.class))
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    @GetMapping("{id}/movies")
    public ResponseEntity<?> findAllCharacters(@PathVariable int id){
        try {
            franchiseService.findAllCharacters(id);
            return ResponseEntity.ok().build();
        } catch (MovieNotFoundException e){
            return ResponseEntity.notFound().build();
        }
    }
}
