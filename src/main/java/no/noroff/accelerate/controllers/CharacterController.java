package no.noroff.accelerate.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate.exceptions.CharacterNotFoundException;
import no.noroff.accelerate.mappers.CharacterMapper;
import no.noroff.accelerate.models.Character;
import no.noroff.accelerate.models.dto.character.CharacterDTO;
import no.noroff.accelerate.models.dto.character.CharacterPostDTO;
import no.noroff.accelerate.services.character.CharacterService;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping(path = "api/v1/characters")
public class CharacterController {
    private final CharacterService characterService;
    private final CharacterMapper characterMapper;


    public CharacterController(CharacterService characterService, CharacterMapper characterMapper) {
        this.characterService = characterService;
        this.characterMapper = characterMapper;
    }

    @GetMapping
    @Operation(summary = "Gets all characters")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class)))
                    }
            )
    })
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(
                characterMapper.characterToCharacterDTO(
                        characterService.findAll()
                )
        );
    }

    @GetMapping("{id}")
    @Operation(summary = "Gets a character by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = CharacterDTO.class))
                    }
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))
            )
    })
    public ResponseEntity<?> findById(@PathVariable int id) {
        try {
            return ResponseEntity.ok(
                    characterMapper.characterToCharacterDTO(
                            characterService.findById(id)
                    )

            );
        } catch (CharacterNotFoundException characterNotFoundException) {
            return ResponseEntity.notFound().build();
        }


    }

    @PostMapping
    @Operation(summary = "Adds a new character")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Created",
                    content = @Content
            )
    })
    public ResponseEntity<?> add(@RequestBody CharacterPostDTO entity) throws URISyntaxException {
        Character character = characterService.add(characterMapper.characterPostDTOToCharacter(entity));
        URI uri = new URI("api/v1/characters/" + character.getId());
        return ResponseEntity.created(uri).build();
    }

    @PutMapping("{id}")
    @Operation(summary = "Updates a character")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    public ResponseEntity<?> update(@RequestBody CharacterDTO entity, @PathVariable int id) {
        if (id != entity.getId())
            return ResponseEntity.notFound().build();
        characterService.update(characterMapper.characterDTOToCharacter(entity));
        return ResponseEntity.noContent().build();
    }
    @DeleteMapping("{id}")
    @Operation(summary = "Deletes a character")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Success",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Not Found",
                    content = @Content
            )
    })
    public ResponseEntity<?> delete(@PathVariable int id ) {
        if(characterService.exists(id)){
            Character c = characterService.findById(id);
            c.getMovies().forEach(m-> {
                m.getCharacters().removeIf(character -> character.equals(c));
            });
            characterService.deleteById(id);
            return ResponseEntity.noContent().build();
        }else {
            return ResponseEntity.notFound().build();
        }
    }

}
