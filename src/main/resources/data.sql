-- Insert data into the Franchise table
INSERT INTO franchise (name, description)
VALUES
    ('Star Wars', 'A legendary space opera franchise.'),
    ('Original Trilogy', 'The original three movies in the Star Wars saga.'),
    ('Prequel Trilogy', 'The prequel movies that provide backstory to the original trilogy.'),
    ('Sequel Trilogy', 'The latest installment in the Star Wars saga.');

-- Insert data into the Movie table
INSERT INTO movie (title, genre, release_Year, director, picture_Url, trailer_Url, franchise_id)
VALUES
    ('Star Wars: Episode IV - A New Hope', 'Sci-Fi', 1977, 'George Lucas', 'image_url_1', 'trailer_url_1', 2),
    ('Star Wars: Episode V - The Empire Strikes Back', 'Sci-Fi', 1980, 'Irvin Kershner', 'image_url_2', 'trailer_url_2', 2),
    ('Star Wars: Episode VI - Return of the Jedi', 'Sci-Fi', 1983, 'Richard Marquand', 'image_url_3', 'trailer_url_3', 2),
    ('Star Wars: Episode I - The Phantom Menace', 'Sci-Fi', 1999, 'George Lucas', 'image_url_4', 'trailer_url_4', 3),
    ('Star Wars: Episode II - Attack of the Clones', 'Sci-Fi', 2002, 'George Lucas', 'image_url_5', 'trailer_url_5', 3),
    ('Star Wars: Episode III - Revenge of the Sith', 'Sci-Fi', 2005, 'George Lucas', 'image_url_6', 'trailer_url_6', 3),
    ('Star Wars: Episode VII - The Force Awakens', 'Sci-Fi', 2015, 'J.J. Abrams', 'image_url_7', 'trailer_url_7', 4),
    ('Star Wars: Episode VIII - The Last Jedi', 'Sci-Fi', 2017, 'Rian Johnson', 'image_url_8', 'trailer_url_8', 4),
    ('Star Wars: Episode IX - The Rise of Skywalker', 'Sci-Fi', 2019, 'J.J. Abrams', 'image_url_9', 'trailer_url_9', 4);


-- Insert data into the Character table
INSERT INTO character (full_Name, alias, gender, picture_Url)
VALUES
    ('Luke Skywalker', 'Farm Boy', 'Male', 'luke_skywalker.jpg'),
    ('Princess Leia Organa', 'Leia', 'Female', 'leia_organa.jpg'),
    ('Han Solo', 'Captain Solo', 'Male', 'han_solo.jpg'),
    ('Darth Vader', 'Anakin Skywalker', 'Male', 'darth_vader.jpg'),
    ('Obi-Wan Kenobi', 'Ben Kenobi', 'Male', 'obi_wan_kenobi.jpg'),
    ('Yoda', 'Master Yoda', 'Male', 'yoda.jpg'),
    ('Chewbacca', 'Chewie', 'Male', 'chewbacca.jpg'),
    ('R2-D2', 'Artoo', 'Droid', 'r2d2.jpg'),
    ('C-3PO', 'See-Threepio', 'Droid', 'c3po.jpg');

-- Insert data into the character_movie junction table to associate characters with movies
INSERT INTO character_movies (character_id, movies_id)
VALUES
    (1, 1), (1, 2), (1, 3),
    (2, 1), (2, 2), (2, 3),
    (3, 1), (3, 2), (3, 3),
    (4, 1), (4, 2), (4, 3),
    (5, 1), (5, 2), (5, 3),
    (5, 4), (5, 5), (5, 6),
    (6, 1), (6, 2), (6, 3),
    (7, 1), (7, 2), (7, 3),
    (8, 1), (8, 2), (8, 3),
    (9, 1), (9, 2), (9, 3);

insert into movie_characters(movie_id, characters_id)
Values
    (1, 1), (2, 1), (3, 1),
    (1, 2), (2, 2), (3, 2),
    (1, 3), (2, 3), (3, 3),
    (1, 4), (2, 4), (3, 4),
    (1, 5), (2, 5), (3, 5),
    (1, 6), (2, 6), (3, 6),
    (1, 7), (2, 7), (3, 7),
    (1, 8), (2, 8), (3, 8),
    (1, 9), (2, 9), (3, 9);